email: 111@ukr.net
pass: d1a2n3i4t5


## Team
Oleksiy Oleksenko,
Maksym Lazarenko,
Vasyl Umanets

## Tasks
Team:
- Code review and commenting 

Oleksiy Oleksenko:
- Classes and methods for Modal windows

Maksym Lazarenko:
- Classes and methods for Cards

Vasyl Umanets:
- General design
- Main functions
- Forms validation
- Fetch API functions
- Filter form design