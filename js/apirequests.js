const API_URL = "https://ajax.test-danit.com/api/v2/cards";

const getAccess = async (userEmail, userPassword) => {
  const response = await fetch(`${API_URL}/login`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ email: userEmail, password: userPassword }),
  });
  // console.log("ffgfg",response.body);
  if (response.status !== 200) {
    alert("Enter correct username or password");
  } else {
    const token = await response.text();
      localStorage.setItem("token", token);
      console.log("token:", token);
    return token;
  }
};

const getAllCards = async () => {
  const response = await fetch(API_URL, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
    const cards = await response.json();
    // console.log(cards);
  return cards;
};

const getCard = async (cardId) => {
  const response = await fetch(`${API_URL}/${cardId}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const card = await response.json();
  // console.log(card);
  return card;
};

const createVisit = async (formData) => {
  const response = await fetch(API_URL, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({ ...formData }),
  });
  return response;
};

const editCard = async (cardId, formData) => {
  const response = await fetch(`${API_URL}/${cardId}`, {
    method: "PUT",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({ ...formData }),
  });

  return response;
};

const deleteCard = async (cardId) => {
  const response = await fetch(`${API_URL}/${cardId}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });

  return response;
};

export { getAccess, createVisit, getAllCards, deleteCard, editCard, getCard };

