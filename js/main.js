import {
  getAccess,
  createVisit,
  getAllCards,
  editCard,
} from "./apirequests.js";
import { VisitDentist, VisitTherapist, VisitCardiologist, Modal } from "./classes.js";

const btnLogin = document.querySelector("#login");
const btnCreate = document.querySelector("#btn-create");
const noitems = document.querySelector(".noitems");
const cardsWrapper = document.querySelector(".cardsWrapper");
let modalWindow;

function tokenChecker(){
  const token = localStorage.getItem("token");
  if(token){
    btnLogin.style.display = 'none';
    btnCreate.style.display = "initial";
    renderAllCadrs();
  }
}

tokenChecker()


//RENDER ALL CARDS ON PAGE
async function renderAllCadrs() {
  document.getElementById("card-row").innerHTML = "";
  let allCards = await getAllCards();
  if (allCards.length < 1) {
    noitems.style.display = "block";
  } else {
    noitems.style.display = "none";
    allCards.forEach((visit) => {
      // console.log(visit);
      if (visit.doctor === "Dentist") {
        const visitCard = new VisitDentist(visit);
        visitCard.render(cardsWrapper);
      } else if (visit.doctor === "Cardiologist") {
        const visitCard = new VisitCardiologist(visit);
        visitCard.render(cardsWrapper);
      } else if (visit.doctor === "Therapist") {
        const visitCard = new VisitTherapist(visit);
        visitCard.render(cardsWrapper);
      }
    });
  }
}

// LOGIN
let modalLogin = document.querySelector("#modalLogin");
modalLogin.addEventListener("submit", (event) => {
  event.preventDefault();
  let email = document.getElementById("floatingInput").value;
  let password = document.getElementById("floatingPassword").value;
  getAccess(email, password).then(() => {
    renderAllCadrs();
  });
  document.getElementById("login-modal-close").click();
  btnLogin.style.display = "none";
  btnCreate.style.display = "initial";
});

// SEARCH
let input = document.getElementById("search");
input.addEventListener("keyup", function () {
  const cards = document.getElementsByClassName("col-sm-4");
  for (let i = 0; i < cards.length; i++) {
    let title = cards[i].querySelector(".card-title");
    if (title.innerText.toUpperCase().indexOf(input.value.toUpperCase()) > -1) {
      cards[i].style.display = "";
    } else {
      cards[i].style.display = "none";
    }
  }
});

//FILTER by Urgency
let selectedUrgencyMenu = document.getElementById("urgency-container");
// console.log(selectedUrgencyMenu);
selectedUrgencyMenu.addEventListener("change", function () {
  let selectedValue = this.value;
  const cards = document.getElementsByClassName("col-sm-4");
  for (let i = 0; i < cards.length; i++) {
    let cardUrgency = cards[i]
      .querySelector(".cardUrgency")
      .innerText.split(" ")[1];
    if (selectedValue === cardUrgency || selectedValue === "") {
      cards[i].style.display = "";
    } else {
      cards[i].style.display = "none";
    }
  }
})

//FILTER by Status
let selectedStatusMenu = document.getElementById("status-container");
selectedStatusMenu.addEventListener("change", function () {
  let selectedValue = this.value;
  const cards = document.getElementsByClassName("col-sm-4");
  for (let i = 0; i < cards.length; i++) {
    let cardStatus = cards[i]
      .querySelector(".card-status")
      .innerText.split(" ")[1];
    if (selectedValue === cardStatus || selectedValue === "") {
      cards[i].style.display = "";
    } else {
      cards[i].style.display = "none";
    }
  }
});


//Create Card
btnCreate.addEventListener("click", function () {
  console.log("Render Modal create card");
  modalWindow = new Modal();
  modalWindow.renderModal();
});

document.addEventListener('click', async (e) => {
  if (e.target.id === "create-btn") {
    e.preventDefault();
    const form = document.querySelector("#newVisitForm");
    console.log("submit form");

    form.classList.add('was-validated');
    // validate fields
    if (form.checkValidity()) {
      let formData = new FormData(form)
      console.log("formData", formData);
      let visitData = formToJson(formData);
      visitData.status = 'Open';
      // console.log("visitData", visitData);
      createVisit(visitData);
      modalWindow.close();
      renderAllCadrs();
    }
  }
})

//Edit Card
document.addEventListener('click', async (e) => {
  if (e.target.id === "edit-btn") {
    e.preventDefault();
    const editForm = document.querySelector("#editVisitForm");
    console.log("submit editing form", editForm);

    editForm.classList.add('was-validated');
    // validate fields
    if (editForm.checkValidity()) {
      let formData = new FormData(editForm);
      console.log("formData", formData);
      let visitData = formToJson(formData);
      console.log(visitData.id, visitData);
      editCard(visitData.id, visitData).then(() => renderAllCadrs());
      document.getElementById("edit-modal-close").click()
      
    }
  }
})


function formToJson(formData) {
  let obj = {};
  for (let [name, value] of formData) {
    if (value) {
      obj[name] = value;
    }
  }
  return obj;
}

