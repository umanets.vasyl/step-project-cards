import {
  getAccess,
  createVisit,
  getAllCards,
  getCard,
  deleteCard,
  editCard,
} from "./apirequests.js";

export class Visit {
  constructor({
    id,
    doctor,
    purpose,
    description,
    urgency,
    clientName,
    status,
  }) {
    this.id = id;
    this.status = status;
    this.doctor = doctor;
    this.purpose = purpose;
    this.description = description;
    this.urgency = urgency;
    this.clientName = clientName;
    this.card = document.createElement("div");
  }

  //Render card
  render(parent) {
    let card_elem = document.createElement("div");
    this.card.classList.add("col-sm-4", "mb-0", "mb-sm-0");
    this.card.setAttribute("id", `card-${this.id}`);

    card_elem.setAttribute("id", `card-elem-${this.id}`);
    card_elem.classList.add(
      "card",
      "border-0",
      "shadow",
      "p-3",
      "mb-5",
      "bg-body-tertiary",
      "rounded",
      "visit-card"
    );

    let cardHeader = document.createElement("div");
    cardHeader.classList.add(
      "d-flex",
      "justify-content-between",
      "align-items-center",
      "border-0",
      "bg-body-tertiary"
    );

    let cardTitle = document.createElement("h6");
    cardTitle.classList.add("card-status");
    cardTitle.innerText = `Status: ${this.status}`;

    let btnClose = document.createElement("button");
    btnClose.classList.add("btn-close");
    btnClose.setAttribute("type", "button");
    btnClose.setAttribute("id", "card-close");
    btnClose.setAttribute("aria-label", "Close");

    let btnEdit = document.createElement("button");
    btnEdit.classList.add("btn", "btn-light", "btn-sm", "rounded-0");
    btnEdit.setAttribute("type", "button");
    btnEdit.setAttribute("id", "card-edit");
    btnEdit.setAttribute("aria-label", "Edit");
    btnEdit.innerHTML = "<i style='font-size:20px' class='fa'>&#xf044;</i>";

    let cardBody = document.createElement("div");
    cardBody.classList.add("pb-0");
    let cardClientName = document.createElement("h5");
    cardClientName.classList.add("card-title");
    cardClientName.innerText = `Patient: ${this.clientName}`;

    let cardDoctor = document.createElement("h6");
    cardDoctor.classList.add("card-subtitle", "mb-4");
    cardDoctor.innerText = `Doctor: ${this.doctor}`;

    let cardUrgency = document.createElement("h6");
    cardUrgency.classList.add("card-subtitle", "mb-4", "cardUrgency");
    cardUrgency.innerText = `Urgency: ${this.urgency}`;

    let cardShowMore = document.createElement("div");
    cardShowMore.classList.add("accordion", "accordion-flush");
    cardShowMore.innerHTML = `<button id="showMore" class="accordion-button collapsed bg-body-tertiary show-more-btn" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-${this.id}" aria-expanded="false" aria-controls="flush-collapseOne">
                    Show more
                </button>
                <div id="collapse-${this.id}" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <ul class="card-list list-group list-group-flush rounded-bottom">
                        <li class="card-list-item list-group-item">Purpose: ${this.purpose}</li>
                        <li class="card-list-item list-group-item">Description: ${this.description}</li>
                    </ul>
                </div>`;

    this.card.append(card_elem);
    card_elem.append(cardHeader);
    cardHeader.append(cardTitle);
    cardHeader.append(btnEdit);
    cardHeader.append(btnClose);
    card_elem.append(cardBody);
    cardBody.append(cardUrgency);
    cardBody.append(cardDoctor);
    cardBody.append(cardClientName);
    cardBody.append(cardShowMore);
    this.cardList = this.card.querySelector(".card-list");
    parent.appendChild(this.card);

    btnClose.addEventListener("click", (e) => {
      e.preventDefault();
      this.delete();
    });

    btnEdit.addEventListener("click", (e) => {
      e.preventDefault();
      // console.log("THIS CLASS ID", this.id);
      this.showModal();
    });
  }

  delete() {
    deleteCard(this.id).then((response) => {
      if (response.ok) {
        const element = document.getElementById(`card-${this.id}`);
        element.remove();
      }
    });
  }

  showModal() {
    let returnedId = this.id;
    let modalEditWindow = new ModalEdit();
    modalEditWindow.renderModalEdit(returnedId);
  }

}

//Child class Dentist
export class VisitDentist extends Visit {
  constructor({
    id,
    status,
    doctor,
    purpose,
    description,
    urgency,
    clientName,
    lastVisit,
  }) {
    super({ id, status, doctor, purpose, description, urgency, clientName });
    this.lastVisit = lastVisit;
  }
  render(parent) {
    super.render(parent);

    this.card.classList.add("bg-dentist");
    this.card.querySelector(".accordion-collapse").classList.add("bg-dentist");
    this.cardList.insertAdjacentHTML(
      "beforeend",
      `<li class="card-list-item list-group-item">Date of last visit: <br>${this.lastVisit}</li>`
    );
    parent.append(this.card);
  }
}

//Child class Therapist
export class VisitTherapist extends Visit {
  constructor({
    id,
    status,
    doctor,
    purpose,
    description,
    urgency,
    clientName,
    age,
  }) {
    super({ id, status, doctor, purpose, description, urgency, clientName });
    this.age = age;
  }

  render(parent) {
    super.render(parent);

    this.card.classList.add("bg-therapist");
    this.card
      .querySelector(".accordion-collapse")
      .classList.add("bg-therapist");
    this.cardList.insertAdjacentHTML(
      "beforeend",
      `<li class="card-list-item list-group-item">Age: ${this.age}</li>`
    );
    parent.append(this.card);
  }
}

//Child class Cardiologist
export class VisitCardiologist extends Visit {
  constructor({
    id,
    status,
    doctor,
    purpose,
    description,
    urgency,
    clientName,
    commonPreasure,
    weightIndex,
    pastDiseases,
    age,
  }) {
    super({ status, id, doctor, purpose, description, urgency, clientName });
    this.commonPreasure = commonPreasure;
    this.weightIndex = weightIndex;
    this.pastDiseases = pastDiseases;
    this.age = age;
  }

  render(parent) {
    super.render(parent);
    this.card.classList.add("bg-cardiologist");
    this.card
      .querySelector(".accordion-collapse")
      .classList.add("bg-cardiologist");
    this.cardList.insertAdjacentHTML(
      "beforeend",
      `
        <li class="card-list-item list-group-item">Basic pressure: ${this.commonPreasure}</li>
        <li class="card-list-item list-group-item">Body mass index: ${this.weightIndex}</li>
        <li class="card-list-item list-group-item">Past Diseases: ${this.pastDiseases}</li>
        <li class="card-list-item list-group-item">Age: ${this.age}</li>
        `
    );
    parent.append(this.card);
  }
}

export class Modal {
  constructor() {}
  renderModal() {
    let body = document.body

    let mod = document.createElement("div")
    mod.classList.add("modal")
    mod.setAttribute("tabindex","-1")
    mod.setAttribute("id","myModal")

    body.append(mod)

    let mod_div1 = document.createElement("div")
    mod_div1.classList.add("modal-dialog")

    mod.append(mod_div1)

        let mod_div2 = document.createElement("div")
        mod_div2.classList.add("modal-content")

        mod_div1.append(mod_div2)

            let mod_div3 = document.createElement("div")
            mod_div3.classList.add("modal-header")

                let mod_title1 = document.createElement("h5")
                mod_title1.classList.add("modal-title")
                mod_title1.textContent = "Create Visit"

                let mod_btn1 = document.createElement("button")
                mod_btn1.setAttribute("type","button")
                mod_btn1.classList.add("btn-close")
                mod_btn1.setAttribute("data-bs-dismiss","modal")
                mod_btn1.setAttribute("aria-label","Close")

                mod_div3.append(mod_title1)
                mod_div3.append(mod_btn1)

            let mod_p1 = document.createElement("p")
            mod_p1.classList.add("invalid-message", "text-center", "text-danger", "mt-2", "mb-0")

            let mod_div4 = document.createElement("div")
            mod_div4.classList.add("modal-body")

            mod_div2.append(mod_div3)
            mod_div2.append(mod_p1)
            mod_div2.append(mod_div4)

                let mod_form = document.createElement("form")
                mod_form.setAttribute("id","newVisitForm")
                mod_form.classList.add("g-3", "needs-validation")
                mod_form.setAttribute("novalidate", "")

                mod_div4.append(mod_form)

                    let form_div1 = document.createElement("div")
                    form_div1.classList.add("mb-2", "col-12")
                    mod_form.append(form_div1)

                        let sel1 = document.createElement("select")
                        sel1.setAttribute("id","selectDoctor")
                        sel1.classList.add("form-select")
                        sel1.setAttribute("aria-label","Status")
                        sel1.setAttribute("name","doctor")

                        form_div1.append(sel1)

                            let sel1_opt1 = document.createElement("option")
                            sel1_opt1.setAttribute("selected", "")
                            sel1_opt1.setAttribute("disabled", "")
                            sel1_opt1.textContent = "Select doctor"
                            
                            let sel1_opt2 = document.createElement("option")
                            sel1_opt2.textContent = "Cardiologist"
                            sel1_opt2.value = "Cardiologist"
                            
                            let sel1_opt3 = document.createElement("option")
                            sel1_opt3.textContent = "Dentist"
                            sel1_opt3.value = "Dentist"
                            
                            let sel1_opt4 = document.createElement("option")
                            sel1_opt4.textContent = "Therapist"
                            sel1_opt4.value = "Therapist"
                            
                            sel1.append(sel1_opt1)
                            sel1.append(sel1_opt2)
                            sel1.append(sel1_opt3)
                            sel1.append(sel1_opt4)


                    let form_div2 = document.createElement("div")
                    form_div2.classList.add("d-none")
                    form_div2.setAttribute("id","forAllDoctors")
                    mod_form.append(form_div2)

                        let form_div2_div1 = document.createElement("div")
                        form_div2_div1.classList.add("col-sm-12")
                        form_div2.append(form_div2_div1)

                            let form_div2_div1_divInp1 = document.createElement("div")
                            form_div2_div1_divInp1.classList.add("mb-2", "form-floating")
                            form_div2_div1.append(form_div2_div1_divInp1)

                                let divInp1_inp = document.createElement("input")
                                divInp1_inp.setAttribute("type","text")
                                divInp1_inp.classList.add("form-control")
                                divInp1_inp.setAttribute("id","visitsPurpose")
                                divInp1_inp.setAttribute("name","purpose")
                                divInp1_inp.setAttribute("placeholder","Purpose of the visit")
                                divInp1_inp.setAttribute("required", "")

                                let divInp1_label = document.createElement("label")
                                divInp1_label.setAttribute("for","visitsPurpose")
                                divInp1_label.classList.add("form-label")
                                divInp1_label.textContent = "Purpose of the visit"

                                let divInp1_div = document.createElement("div")
                                divInp1_div.classList.add("invalid-feedback")
                                divInp1_div.textContent = "Can't be empty!"

                                form_div2_div1_divInp1.append(divInp1_inp)
                                form_div2_div1_divInp1.append(divInp1_label)
                                form_div2_div1_divInp1.append(divInp1_div)
                            
                            let form_div2_div1_divsel1 = document.createElement("div")
                            form_div2_div1_divsel1.classList.add("mb-2")
                            form_div2_div1.append(form_div2_div1_divsel1)

                                let divsel1_sel1 = document.createElement("select")
                                divsel1_sel1.setAttribute("id","select-urgency")
                                divsel1_sel1.classList.add("form-select")
                                divsel1_sel1.setAttribute("aria-label","Urgency")
                                divsel1_sel1.setAttribute("name","urgency")
                                divsel1_sel1.setAttribute("required", "")

                                    let divsel1_opt1 = document.createElement("option")
                                    divsel1_opt1.setAttribute("selected", "")
                                    divsel1_opt1.setAttribute("disabled", "")
                                    divsel1_opt1.textContent = "Urgency"

                                    let divsel1_opt2 = document.createElement("option")
                                    divsel1_opt2.setAttribute("value", "high")
                                    divsel1_opt2.textContent = "High"

                                    let divsel1_opt3 = document.createElement("option")
                                    divsel1_opt2.setAttribute("value", "middle")
                                    divsel1_opt3.textContent = "Middle"

                                    let divsel1_opt4 = document.createElement("option")
                                    divsel1_opt2.setAttribute("value", "low")
                                    divsel1_opt4.textContent = "Low"

                                    divsel1_sel1.append(divsel1_opt1)
                                    divsel1_sel1.append(divsel1_opt2)
                                    divsel1_sel1.append(divsel1_opt3)
                                    divsel1_sel1.append(divsel1_opt4)

                                let divsel1_div = document.createElement("div")
                                divsel1_div.classList.add("invalid-feedback")
                                divsel1_div.textContent = "Choose one"

                                form_div2_div1_divsel1.append(divsel1_sel1)
                                form_div2_div1_divsel1.append(divsel1_div)

                        let form_div2_div2 = document.createElement("div")
                        form_div2_div2.classList.add("col-sm-12")
                        form_div2.append(form_div2_div2)

                            let form_div2_div2_divTxt1 = document.createElement("div")
                            form_div2_div2_divTxt1.classList.add("mb-2", "form-floating")
                            form_div2_div2.append(form_div2_div2_divTxt1)

                                let divTxt1_txtArea = document.createElement("textarea")
                                divTxt1_txtArea.classList.add("form-control")
                                divTxt1_txtArea.setAttribute("id","shortDescription")
                                divTxt1_txtArea.setAttribute("name","description")
                                divTxt1_txtArea.setAttribute("placeholder","Short description of the visit")
                                divTxt1_txtArea.style.height = "80px"
                                divTxt1_txtArea.setAttribute("required", "")

                                let divTxt1_label = document.createElement("label")
                                divTxt1_label.setAttribute("for","shortDescription")
                                divTxt1_label.classList.add("form-label")
                                divTxt1_label.textContent = "Description of the visit"

                                let divTxt1_div = document.createElement("div")
                                divTxt1_div.classList.add("invalid-feedback")
                                divTxt1_div.textContent = "Can't be empty!"

                                form_div2_div2_divTxt1.append(divTxt1_txtArea)
                                form_div2_div2_divTxt1.append(divTxt1_label)
                                form_div2_div2_divTxt1.append(divTxt1_div)

                        let form_div2_div3 = document.createElement("div")
                        form_div2_div3.classList.add("col-md-12")
                        form_div2.append(form_div2_div3)
    
                            let form_div2_div3_divInp2 = document.createElement("div")
                            form_div2_div3_divInp2.classList.add("mb-2", "form-floating")
                            form_div2_div3.append(form_div2_div3_divInp2)
    
                                let divInp2_inp = document.createElement("input")
                                divInp2_inp.setAttribute("type","text")
                                divInp2_inp.classList.add("form-control")
                                divInp2_inp.setAttribute("id","clientName")
                                divInp2_inp.setAttribute("name","clientName")
                                divInp2_inp.setAttribute("placeholder","Patient name")
                                divInp2_inp.setAttribute("required", "")
    
                                let divInp2_label = document.createElement("label")
                                divInp2_label.setAttribute("for","clientName")
                                divInp2_label.classList.add("form-label")
                                divInp2_label.textContent = "Patient name"
    
                                let divInp2_div = document.createElement("div")
                                divInp2_div.classList.add("invalid-feedback")
                                divInp2_div.textContent = "Please enter name!"
    
                                form_div2_div3_divInp2.append(divInp2_inp)
                                form_div2_div3_divInp2.append(divInp2_label)
                                form_div2_div3_divInp2.append(divInp2_div)

                    let form_div3 = document.createElement("div")
                    form_div3.classList.add("row")
                    form_div3.setAttribute("id","additional")
                    mod_form.append(form_div3)

                    let form_div4 = document.createElement("div")
                    form_div4.classList.add("mb-3", "d-flex", "justify-content-end")
                    mod_form.append(form_div4)

                        let form_div4_btn = document.createElement("button")
                        form_div4_btn.setAttribute("type","submit")
                        form_div4_btn.setAttribute("id","create-btn")
                        form_div4_btn.classList.add("btn", "btn-primary", "ms-3", "d-none")
                        form_div4_btn.textContent = "Create visit";

                        form_div4.append(form_div4_btn)

        const form = document.querySelector("#newVisitForm");
        const allDoctorsBlock = form.querySelector("#forAllDoctors");
        const additionalBlock = form.querySelector("#additional");

        const saveBtn = form.querySelector("#create-btn");
        form.addEventListener("change", (e) => {
        if (e.target === form.querySelector("#selectDoctor")) {

            function delAdd(){
              let add = document.querySelectorAll(".delAdd")
              add.forEach((e) => {
                e.remove();
              })
            };
            delAdd()

            if (e.target.value === "Cardiologist") {
                allDoctorsBlock.classList.remove("d-none");
                saveBtn.classList.remove("d-none");

                let card_div1 = document.createElement("div")
                card_div1.classList.add("col-sm-12", "delAdd")
                additionalBlock.append(card_div1)

                    let card_div1_1 = document.createElement("div")
                    card_div1_1.classList.add("mb-2", "form-floating")
                    card_div1.append(card_div1_1)

                        let card_inp1 = document.createElement("input")
                        card_inp1.setAttribute("type","number")
                        card_inp1.classList.add("form-control")
                        card_inp1.setAttribute("name","age")
                        card_inp1.setAttribute("id","age")
                        card_inp1.setAttribute("placeholder","Age")
                        card_inp1.setAttribute("min","1")
                        card_inp1.setAttribute("max","110")
                        card_inp1.setAttribute("required","")

                        let card_label1 = document.createElement("label")
                        card_label1.setAttribute("for","age")
                        card_label1.classList.add("form-label")
                        card_label1.textContent = "Age"

                        let card_invalDiv1 = document.createElement("div")
                        card_invalDiv1.classList.add("invalid-feedback")
                        card_invalDiv1.textContent = "Enter your age!"

                        card_div1_1.append(card_inp1)
                        card_div1_1.append(card_label1)
                        card_div1_1.append(card_invalDiv1)

                let card_div2 = document.createElement("div")
                card_div2.classList.add("col-sm-12", "delAdd")
                additionalBlock.append(card_div2)
        
                    let card_div2_1 = document.createElement("div")
                    card_div2_1.classList.add("mb-2", "form-floating")
                    card_div2.append(card_div2_1)
        
                        let card_inp2 = document.createElement("input")
                        card_inp2.setAttribute("type","number")
                        card_inp2.classList.add("form-control")
                        card_inp2.setAttribute("name","weightIndex")
                        card_inp2.setAttribute("id","weightIndex")
                        card_inp2.setAttribute("placeholder","Body mass index")
                        card_inp2.setAttribute("min","18")
                        card_inp2.setAttribute("max","55")
                        card_inp2.setAttribute("required","")
        
                        let card_label2 = document.createElement("label")
                        card_label2.setAttribute("for","weightIndex")
                        card_label2.classList.add("form-label")
                        card_label2.textContent = "Body mass index"
        
                        let card_invalDiv2 = document.createElement("div")
                        card_invalDiv2.classList.add("invalid-feedback")
                        card_invalDiv2.textContent = "Enter a number!"
        
                        card_div2_1.append(card_inp2)
                        card_div2_1.append(card_label2)
                        card_div2_1.append(card_invalDiv2)

                let card_div3 = document.createElement("div")
                card_div3.classList.add("col-12", "delAdd")
                additionalBlock.append(card_div3)
                
                    let card_div3_1 = document.createElement("div")
                    card_div3_1.classList.add("mb-2", "form-floating")
                    card_div3.append(card_div3_1)
                
                        let card_inp3 = document.createElement("input")
                        card_inp3.setAttribute("type","text")
                        card_inp3.classList.add("form-control")
                        card_inp3.setAttribute("name","commonPreasure")
                        card_inp3.setAttribute("id","commonPreasure")
                        card_inp3.setAttribute("placeholder","Common Preasure")
                        card_inp3.setAttribute("required","")
                
                        let card_label3 = document.createElement("label")
                        card_label3.setAttribute("for","commonPreasure")
                        card_label3.classList.add("form-label")
                        card_label3.textContent = "Common preasure"
                
                        let card_invalDiv3 = document.createElement("div")
                        card_invalDiv3.classList.add("invalid-feedback")
                        card_invalDiv3.textContent = "Can't be empty!"
                
                        card_div3_1.append(card_inp3)
                        card_div3_1.append(card_label3)
                        card_div3_1.append(card_invalDiv3)

                let card_div4 = document.createElement("div")
                card_div4.classList.add("col-12", "delAdd")
                additionalBlock.append(card_div4)
                        
                    let card_div4_1 = document.createElement("div")
                    card_div4_1.classList.add("mb-2", "form-floating")
                    card_div4.append(card_div4_1)
                        
                        let card_inp4 = document.createElement("input")
                        card_inp4.setAttribute("type","text")
                        card_inp4.classList.add("form-control")
                        card_inp4.setAttribute("name","pastDiseases")
                        card_inp4.setAttribute("id","diseases")
                        card_inp4.setAttribute("placeholder","Past diseases")
                        card_inp4.setAttribute("required","")
                        
                        let card_label4 = document.createElement("label")
                        card_label4.setAttribute("for","pastDiseases")
                        card_label4.classList.add("form-label")
                        card_label4.textContent = "Past diseases"
                        
                        let card_invalDiv4 = document.createElement("div")
                        card_invalDiv4.classList.add("invalid-feedback")
                        card_invalDiv4.textContent = "Can't be empty!"
                        
                        card_div4_1.append(card_inp4)
                        card_div4_1.append(card_label4)
                        card_div4_1.append(card_invalDiv4)

            } else if (e.target.value === "Dentist") {
                allDoctorsBlock.classList.remove("d-none");
                saveBtn.classList.remove("d-none");

                let dent_div1 = document.createElement("div")
                dent_div1.classList.add("col-12", "delAdd")
                additionalBlock.append(dent_div1)
                        
                    let dent_div1_1 = document.createElement("div")
                    dent_div1_1.classList.add("mb-2", "form-floating")
                    dent_div1.append(dent_div1_1)
                        
                        let dent_inp1 = document.createElement("input")
                        dent_inp1.setAttribute("type","date")
                        dent_inp1.classList.add("form-control")
                        dent_inp1.setAttribute("name","lastVisit")
                        dent_inp1.setAttribute("id","date")
                        dent_inp1.setAttribute("placeholder","Date of last visit")
                        dent_inp1.setAttribute("required","")
                        
                        let dent_label1 = document.createElement("label")
                        dent_label1.setAttribute("for","date")
                        dent_label1.classList.add("form-label")
                        dent_label1.textContent = "Date of last visit"
                        
                        let dent_invalDiv1 = document.createElement("div")
                        dent_invalDiv1.classList.add("invalid-feedback")
                        dent_invalDiv1.textContent = "Choose date!"
                        
                        dent_div1_1.append(dent_inp1)
                        dent_div1_1.append(dent_label1)
                        dent_div1_1.append(dent_invalDiv1)
            } else if (e.target.value === "Therapist") {
                allDoctorsBlock.classList.remove("d-none");
                saveBtn.classList.remove("d-none");

                let ther_div1 = document.createElement("div")
                ther_div1.classList.add("col-12", "delAdd")
                additionalBlock.append(ther_div1)
                        
                    let ther_div1_1 = document.createElement("div")
                    ther_div1_1.classList.add("mb-2", "form-floating")
                    ther_div1.append(ther_div1_1)
                        
                        let ther_inp1 = document.createElement("input")
                        ther_inp1.setAttribute("type","number")
                        ther_inp1.classList.add("form-control")
                        ther_inp1.setAttribute("name","age")
                        ther_inp1.setAttribute("id","age")
                        ther_inp1.setAttribute("placeholder","Age")
                        ther_inp1.setAttribute("min","1")
                        ther_inp1.setAttribute("max","110")
                        ther_inp1.setAttribute("required","")
                        
                        let ther_label1 = document.createElement("label")
                        ther_label1.setAttribute("for","age")
                        ther_label1.classList.add("form-label")
                        ther_label1.textContent = "Age"
                        
                        let ther_invalDiv1 = document.createElement("div")
                        ther_invalDiv1.classList.add("invalid-feedback")
                        ther_invalDiv1.textContent = "Enter patient age!"
                        
                        ther_div1_1.append(ther_inp1)
                        ther_div1_1.append(ther_label1)
                        ther_div1_1.append(ther_invalDiv1)
            }
        }
    });

    this.modal = new bootstrap.Modal("#myModal", {
      keyboard: false,
    });
    this.modal.show();
    this.modal._element.addEventListener("hidden.bs.modal", (event) =>
      event.target.remove()
    );
  }

  close() {
    this.modal.hide();
  }
}

export class ModalEdit {
  constructor(fullcard) {
    this.fullcard = fullcard;
  }

  renderModalEdit(idCard) {
    getCard(idCard).then((response) => {
      if (response.doctor == "Cardiologist") {
        console.log(response.doctor);
        document.body.insertAdjacentHTML(
          "beforeend",
          `
          <div class="modal" tabindex="-1" id="modalEdit">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Edit Visit</h5>
                <button
                  type="button"
                  class="btn-close"
                  id="edit-modal-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <p class="invalid-message text-center text-danger mt-2 mb-0"></p>
              <div class="modal-body">
                <form id="editVisitForm" class="g-3 needs-validation" novalidate>
                  <div class="col-12">
                    <div class="mb-2 form-floating">
                    <input
                        type="hidden"
                        class="form-control"
                        name="doctor"
                        id="doctor"
                        placeholder="doctor"
                        value="${response.doctor}"
                        
                      />
                      <input
                        
                        class="form-control"
                        name="doctor"
                        id="doctor"
                        placeholder="doctor"
                        value="${response.doctor}"
                        disabled
                      />
                      <label for="doctor" class="form-label">Doctor</label>
                      <div class="invalid-feedback">Can't be empty!</div>
                    </div>
    
                    <div class="mb-2 form-floating">
                      <input
                      type="hidden"
                        class="form-control"
                        name="id"
                        id="cardid"
                        placeholder="cardid"
                        value="${parseInt(response.id)}"
                      />
                      <label for="id" class="form-label">ID</label>
                      <div class="invalid-feedback">Can't be empty!</div>
                    </div>
                  </div>
    
                 
                    <div class="col-sm-12">
                      <div class="mb-2 form-floating">
                        <input
                          type="text"
                          class="form-control"
                          id="visitsPurpose"
                          name="purpose"
                          placeholder="Purpose of the visit"
                          value="${response.purpose}"
                          required
                        />
                        <label for="visitsPurpose" class="form-label"
                          >Purpose of the visit</label
                        >
                        <div class="invalid-feedback">Can't be empty!</div>
                      </div>
                      <div class="mb-2">
                        <select
                          id="select-urgency"
                          class="form-select"
                          aria-label="Urgency"
                          name="urgency"
                          required
                          
                        >
                          <option value="${response.urgency}" selected >${
            response.urgency
          }</option>
                          <option value="high">high</option>
                          <option value="middle">middle</option>
                          <option value="low">low</option>
                        </select>
                        
                        <div class="invalid-feedback">Choose one</div>
                      </div>
    
                      <div class="mb-2">
                        <select
                          id="select-status"
                          class="form-select"
                          aria-label="Status"
                          name="status"
                          required
                          
                        >
                          <option value="${response.status}" selected >${
            response.status
          }</option>
                          <option value="Open">Open</option>
                          <option value="Done">Done</option>
                        </select>
                        <div class="invalid-feedback">Choose one</div>
                      </div>
                    </div>
    
                      <div class="mb-2 form-floating">
                        <input
                          type="textarea"
                          class="form-control"
                          name="description"
                          id="shortDescription"
                          placeholder="Short description of the visit"
                          required
                          value="${response.description}"
                        />
                        <label for="shortDescription" class="form-label"
                          >Description of the visit</label
                        >
                        <div class="invalid-feedback">Can't be empty!</div>
                      </div>
                    <div class="col-12">
                      <div class="mb-2 form-floating">
                        <input
                          type="text"
                          class="form-control"
                          id="clientName"
                          name="clientName"
                          placeholder="Patient name"
                          required
                          value="${response.clientName}"
                        />
                        <label for="clientName" class="form-label"
                          >Patient name</label
                        >
                        <div class="invalid-feedback">Please enter name!</div>
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="mb-2 form-floating">
                        <input
                          type="number"
                          class="form-control"
                          name="age"
                          id="age"
                          placeholder="Age"
                          min="1"
                          max="110"
                          required
                          value="${response.age}"
                        />
                        <label for="age" class="form-label">Age</label>
                        <div class="invalid-feedback">Enter your age</div>
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="mb-2 form-floating">
                        <input
                          type="number"
                          class="form-control"
                          name="weightIndex"
                          id="weightIndex"
                          placeholder="Body mass index"
                          min="18"
                          max="55"
                          required
                          value="${response.weightIndex}"
                        />
                        <label for="bmi" class="form-label">Body mass index</label>
                        <div class="invalid-feedback">Enter a number</div>
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="mb-2 form-floating">
                        <input
                          type="text"
                          class="form-control"
                          id="commonPreasure"
                          name="commonPreasure"
                          placeholder="Common preasure"
                          required
                          value="${response.commonPreasure}"
                        />
                        <label for="commonPreasure" class="form-label"
                          >Common preasure</label
                        >
                        <div class="invalid-feedback">Can't be empty!</div>
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="mb-2 form-floating">
                        <input
                          type="text"
                          class="form-control"
                          name="pastDiseases"
                          id="diseases"
                          placeholder="Past diseases"
                          required
                          value="${response.pastDiseases}"
                        />
                        <label for="pastDiseases" class="form-label"
                          >Past diseases</label
                        >
                        <div class="invalid-feedback">Can't be empty!</div>
                      </div>
                    </div>
    
                  <div class="mb-3 d-flex justify-content-end">
                    <button
                      type="submit"
                      id="edit-btn"
                      class="btn btn-primary ms-3">
                      Submit editing
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
              `
        );
      } else if (response.doctor == "Dentist") {
        console.log(response.doctor);
        document.body.insertAdjacentHTML(
          "beforeend",
          `
          <div class="modal" tabindex="-1" id="modalEdit">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Edit Visit</h5>
                <button
                  type="button"
                  class="btn-close"
                  id="edit-modal-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <p class="invalid-message text-center text-danger mt-2 mb-0"></p>
              <div class="modal-body">
                <form id="editVisitForm" class="g-3 needs-validation" novalidate>
                  <div class="col-12">
                    <div class="mb-2 form-floating">
                    <input
                        type="hidden"
                        class="form-control"
                        name="doctor"
                        id="doctor"
                        placeholder="doctor"
                        value="${response.doctor}"
                        
                      />
                      <input
                        
                        class="form-control"
                        name="doctor"
                        id="doctor"
                        placeholder="doctor"
                        value="${response.doctor}"
                        disabled
                      />
                      <label for="doctor" class="form-label">Doctor</label>
                      <div class="invalid-feedback">Can't be empty!</div>
                    </div>
    
                    <div class="mb-2 form-floating">
                      <input
                      type="hidden"
                        class="form-control"
                        name="id"
                        id="cardid"
                        placeholder="cardid"
                        value="${parseInt(response.id)}"
                      />
                      <label for="id" class="form-label">ID</label>
                      <div class="invalid-feedback">Can't be empty!</div>
                    </div>
                  </div>
    
                 
                    <div class="col-sm-12">
                      <div class="mb-2 form-floating">
                        <input
                          type="text"
                          class="form-control"
                          id="visitsPurpose"
                          name="purpose"
                          placeholder="Purpose of the visit"
                          value="${response.purpose}"
                          required
                        />
                        <label for="visitsPurpose" class="form-label"
                          >Purpose of the visit</label
                        >
                        <div class="invalid-feedback">Can't be empty!</div>
                      </div>
                      <div class="mb-2">
                        <select
                          id="select-urgency"
                          class="form-select"
                          aria-label="Urgency"
                          name="urgency"
                          required
                          
                        >
                          <option value="${response.urgency}" selected >${
            response.urgency
          }</option>
                          <option value="high">high</option>
                          <option value="middle">middle</option>
                          <option value="low">low</option>
                        </select>
                        
                        <div class="invalid-feedback">Choose one</div>
                      </div>
    
                      <div class="mb-2">
                        <select
                          id="select-status"
                          class="form-select"
                          aria-label="Status"
                          name="status"
                          required
                          
                        >
                          <option value="${response.status}" selected >${
            response.status
          }</option>
                          <option value="Open">Open</option>
                          <option value="Done">Done</option>
                        </select>
                        <div class="invalid-feedback">Choose one</div>
                      </div>
                    </div>
    
                      <div class="mb-2 form-floating">
                        <input
                          type="textarea"
                          class="form-control"
                          name="description"
                          id="shortDescription"
                          placeholder="Short description of the visit"
                          required
                          value="${response.description}"
                        />
                        <label for="shortDescription" class="form-label"
                          >Description of the visit</label
                        >
                        <div class="invalid-feedback">Can't be empty!</div>
                      </div>
                    <div class="col-12">
                      <div class="mb-2 form-floating">
                        <input
                          type="text"
                          class="form-control"
                          id="clientName"
                          name="clientName"
                          placeholder="Patient name"
                          required
                          value="${response.clientName}"
                        />
                        <label for="clientName" class="form-label"
                          >Patient name</label
                        >
                        <div class="invalid-feedback">Please enter name!</div>
                      </div>
                    </div>
                    <div class="col-12">
                            <div class="mb-2 form-floating">
                                <input type="date"  class="form-control" name="lastVisit" id="date" placeholder="Date of last visit" required value="${
                                  response.lastVisit
                                }">
                                <label for="date" class="form-label">Date of last visit</label>
                                <div class="invalid-feedback">
                                    Choose date
                                </div>
                            </div>
                        </div>    
                  <div class="mb-3 d-flex justify-content-end">
                    <button
                      type="submit"
                      id="edit-btn"
                      class="btn btn-primary ms-3">
                      Submit editing
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
              `
        );
      } else if (response.doctor == "Therapist") {
        console.log(response.doctor);
        document.body.insertAdjacentHTML(
          "beforeend",
          `
          <div class="modal" tabindex="-1" id="modalEdit">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Edit Visit</h5>
                <button
                  type="button"
                  class="btn-close"
                  id="edit-modal-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <p class="invalid-message text-center text-danger mt-2 mb-0"></p>
              <div class="modal-body">
                <form id="editVisitForm" class="g-3 needs-validation" novalidate>
                  <div class="col-12">
                    <div class="mb-2 form-floating">
                    <input
                        type="hidden"
                        class="form-control"
                        name="doctor"
                        id="doctor"
                        placeholder="doctor"
                        value="${response.doctor}"
                        
                      />
                      <input
                        
                        class="form-control"
                        name="doctor"
                        id="doctor"
                        placeholder="doctor"
                        value="${response.doctor}"
                        disabled
                      />
                      <label for="doctor" class="form-label">Doctor</label>
                      <div class="invalid-feedback">Can't be empty!</div>
                    </div>
    
                    <div class="mb-2 form-floating">
                      <input
                      type="hidden"
                        class="form-control"
                        name="id"
                        id="cardid"
                        placeholder="cardid"
                        value="${parseInt(response.id)}"
                      />
                      <label for="id" class="form-label">ID</label>
                      <div class="invalid-feedback">Can't be empty!</div>
                    </div>
                  </div>
    
                 
                    <div class="col-sm-12">
                      <div class="mb-2 form-floating">
                        <input
                          type="text"
                          class="form-control"
                          id="visitsPurpose"
                          name="purpose"
                          placeholder="Purpose of the visit"
                          value="${response.purpose}"
                          required
                        />
                        <label for="visitsPurpose" class="form-label"
                          >Purpose of the visit</label
                        >
                        <div class="invalid-feedback">Can't be empty!</div>
                      </div>
                      <div class="mb-2">
                        <select
                          id="select-urgency"
                          class="form-select"
                          aria-label="Urgency"
                          name="urgency"
                          required
                          
                        >
                          <option value="${response.urgency}" selected >${
            response.urgency
          }</option>
                          <option value="high">high</option>
                          <option value="middle">middle</option>
                          <option value="low">low</option>
                        </select>
                        
                        <div class="invalid-feedback">Choose one</div>
                      </div>
    
                      <div class="mb-2">
                        <select
                          id="select-status"
                          class="form-select"
                          aria-label="Status"
                          name="status"
                          required
                          
                        >
                          <option value="${response.status}" selected >${
            response.status
          }</option>
                          <option value="Open">Open</option>
                          <option value="Done">Done</option>
                        </select>
                        <div class="invalid-feedback">Choose one</div>
                      </div>
                    </div>
    
                      <div class="mb-2 form-floating">
                        <input
                          type="textarea"
                          class="form-control"
                          name="description"
                          id="shortDescription"
                          placeholder="Short description of the visit"
                          required
                          value="${response.description}"
                        />
                        <label for="shortDescription" class="form-label"
                          >Description of the visit</label
                        >
                        <div class="invalid-feedback">Can't be empty!</div>
                      </div>
                    <div class="col-12">
                      <div class="mb-2 form-floating">
                        <input
                          type="text"
                          class="form-control"
                          id="clientName"
                          name="clientName"
                          placeholder="Patient name"
                          required
                          value="${response.clientName}"
                        />
                        <label for="clientName" class="form-label"
                          >Patient name</label
                        >
                        <div class="invalid-feedback">Please enter name!</div>
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="mb-2 form-floating">
                        <input
                          type="number"
                          class="form-control"
                          name="age"
                          id="age"
                          placeholder="Age"
                          min="1"
                          max="110"
                          required
                          value="${response.age}"
                        />
                        <label for="age" class="form-label">Age</label>
                        <div class="invalid-feedback">Enter your age</div>
                      </div>
                    </div>
                  <div class="mb-3 d-flex justify-content-end">
                    <button
                      type="submit"
                      id="edit-btn"
                      class="btn btn-primary ms-3">
                      Submit editing
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
              `
        );
      }

      this.modal = new bootstrap.Modal("#modalEdit", {
        keyboard: false,
      });
      this.modal.show();
      this.modal._element.addEventListener("hidden.bs.modal", (event) =>
        event.target.remove()
      );
    });
  }

  close() {
    this.modal.hide();
  }
}
